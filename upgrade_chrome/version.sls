# -*- coding: utf-8 -*-
# vim: ft=sls

{% from (tpldir + "/map.jinja") import chrome_settings with context %}

{% if salt['cmd.run'](''~ chrome_settings.config.commands.detector ~'', shell='powershell') %}
{% set chrome_current_path = salt['cmd.run'](''~ chrome_settings.config.commands.chrome_path ~'', shell='powershell') %}
{% set CHROME_VERSION = salt['cmd.run'](''~ chrome_settings.config.commands.version ~'', shell='powershell') %}

chrome_exist:
  test.show_notification:
    - text: 'Chrome is present {{ chrome_current_path }} and chrome version {{ CHROME_VERSION }}'
{% else %}
chrome_doesnt_exist:
  test.show_notification:
    - text: 'Chrome is not present'
{% endif %}
