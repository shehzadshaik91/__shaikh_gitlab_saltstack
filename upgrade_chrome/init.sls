# -*- coding: utf-8 -*-
# vim: ft=sls

{% from (tpldir + "/map.jinja") import chrome_settings with context %}

{% set OSFAMILY = salt['grains.get']('os_family') %}

{% if OSFAMILY == "Windows" %}
include:
  - {{ slspath }}.upgrade
  - {{ slspath }}.version

{% else %}
non_windows_os:
  test.show_notification:
    - text: 'State is only availble on windows system'

{% endif %}

