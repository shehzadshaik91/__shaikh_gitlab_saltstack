# -*- coding: utf-8 -*-
# vim: ft=sls

{% from (tpldir + "/map.jinja") import chrome_settings with context %}

{% set CHROME_VERSION = salt['cmd.run'](''~ chrome_settings.config.commands.version ~'', shell='powershell') %}

{% if salt['cmd.run'](''~ chrome_settings.config.commands.detector ~'', shell='powershell') %}
create_download_dir:
  file.directory:
    - name: {{ chrome_settings.config.download.location }}

{%  for process in chrome_settings.config.processes %}
pre_stop_process_{{ process }}:
  cmd.run:
    - name: Stop-Process -Name {{ process }} -Force -ErrorAction Ignore
    - shell: powershell
    - env:
      - ExecutionPolicy: "Bypass"
    - onlyif:
      - tasklist /FI "IMAGENAME eq {{ process }}.exe" 2>NUL | find /I /N "{{ process }}.exe">NUL
{%  endfor %}

download_chrome_msi:
  file.managed:
    - name: {{ chrome_settings.config.download.location }}\{{ chrome_settings.config.filename }}
    - source: {{ chrome_settings.config.download.installer }}
    - makedirs: true
    - skip_verify: true
    - require:
      - create_download_dir

upgrade_chrome_notification:
  test.show_notification:
    - text: 'Updating chrome version {{ CHROME_VERSION }} to latest.'
    - require:
      - download_chrome_msi

upgrade_chrome_browser:
  cmd.run:
    - name: msiexec /i {{ chrome_settings.config.download.location }}\{{ chrome_settings.config.filename }}
    - env:
      - ExecutionPolicy: "Bypass"
    - require:
      - download_chrome_msi

{%  for process in chrome_settings.config.processes %}
{%   if process == "GoogleUpdate" %}
post_stop_process_{{ process }}:
  cmd.run:
    - name: Stop-Process -Name {{ process }} -Force -ErrorAction Ignore
    - shell: powershell
    - env:
      - ExecutionPolicy: "Bypass"
    - onlyif:
      - tasklist /FI "IMAGENAME eq {{ process }}.exe" 2>NUL | find /I /N "{{ process }}.exe">NUL
    - require:
      - upgrade_chrome_browser
{%   endif %}
{%  endfor %}

{%  for service in chrome_settings.config.services %}
stop_chrome_service_{{ service }}:
  cmd.run:
    - name: |
        Stop-Service -Name {{ service }} -Force -ErrorAction Ignore
        Set-Service -Name {{ service }} -StartupType Manual -ErrorAction Ignore
    - shell: powershell
    - env:
      - ExecutionPolicy: "Bypass"
    - require:
      - upgrade_chrome_browser
{%  endfor %}
{#
remove_files_dir:
  file.absent:
    - names:
      - {{ chrome_settings.config.download.location }}
      - {{ chrome_settings.config.shortcut }}
#}
{% else %}
chrome_not_present:
  test.show_notification:
    - text: 'Chrome browser not found, nothing to upgrade.'
{% endif %}
