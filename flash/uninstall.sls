# -*- coding: utf-8 -*-
# vim: ft=sls

{% from (tpldir + "/map.jinja") import flash_settings with context %}

{% set OSFAMILY = salt['grains.get']('os_family') %}

{% if OSFAMILY == "Windows" %}

{%  set isFlashPresent = salt['cmd.retcode']('Test-Path '~ flash_settings.config.path ~'', shell='powershell') %}

{%  if isFlashPresent %}
{% for process in flash_settings.browser.processes %}
stop_broswer_{{ process }}:
  cmd.run:
    - name: Stop-Process -Name {{ process }} -Force -ErrorAction Ignore
    - shell: powershell
    - env:
      - ExecutionPolicy: "bypass"
    - onlyif:
      - tasklist /FI "IMAGENAME eq {{ process }}.exe" 2>NUL | find /I /N "{{ process }}.exe">NUL

{% endfor %}

copy_flash_uninstaller:
  file.managed:
    name: {{ flash_settings.uninstaller.name }}
    source: {{ flash_settings.uninstaller.source }}
    makedirs: true

run_flash_uninstaller:
  cmd.run:
    - name: {{ flash_settings.uninstaller.name }} -uninstall
    - require:
      - file: copy_flash_uninstaller

remove_flash_files:
  file.absent:
    - name: {{ flash_settings.config.path }}

{%  else %}
skip_flash_uninstall:
  test.show_notification:
    - text: "Adobe Flash Player not found on the system, skiping uninstaller."
{% endif %}
{% else %}
skip_non_windows:
  test.show_notification:
    - text: "Adobe Flash Player uninstallation state is not supported on {{ OSFAMILY }}."
{% endif %}
