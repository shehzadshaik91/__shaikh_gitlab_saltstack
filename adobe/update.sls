# _*_ coding: utf-8 _*_
# vim: ft=sls

{% from (tpldir + "/map.jinja") import adobe_settings with context %}

{% set OSFAMILY = salt['grains.get']('os_family') %}
{% if OSFAMILY == "Windows" %}
{%  set isAdobePresent = salt['cmd.run'](''~ adobe_settings.commands.detect ~', shell=powershell') %}
{%  if isAdobePresent == "True" %}
{%   if adobe_settings.config.process is defined %}
{%    for process in adobe_settings.config.processes %}
stop_adobe_{{ process }}:
  cmd.run:
    - name: Stop-Process -Name {{ process }} -Force -ErrorAction Ignore
    - shell: powershell
    - env:
      - ExecutionPolicy: "bypass"
    - onlyif:
      - tasklist /FI "IMAGENAME eq {{ process }}.exe" 2>NUL | find /I /N "{{ process }}.exe">NUL

{%    endfor %}
{%   endif %}

{%   if adobe_settings.config.service is defined %}
{%    for service in adobe_settings.config.service %}
stop_adobe_{{ service }}:
  cmd.run:
    - name: Stop-Service -Name {{ service }} -Force -ErrorAction Ignore
    - shell: powershell
    - env:
      - ExecutionPolicy: "bypass" 

{%    endfor %}
{%   endif %}

create_download_dir:
  file.directory:
    - name: {{ adobe_settings.download.location }}
    
download_reader_msi:
  file.managed:
    - name: {{ adobe_settings.download.location }}\{{ adobe_settings.config.filename }}
    - source: {{ adobe_settings.config.source }}
    - makedirs: True

// command needs to updated
update_adobe_reader:
  cmd.run:
    - name: {{ adobe_settings.download.location }}\{{ adobe_settings.config.filename }} /sAll /norestart ALLUSERS=1 EULA=1
    - require:
      - download_reader_msi
      
{% else %}
skip_adobe_notfound:
  test.show_notification:
    - text: "Adobe not found on the system, skiping update."

{% else %}
skip_non_windows:
  test.show_notification:
    - text: "Adobe update state is not supported on {{ OSFAMILY }}."
{% endif %}

