
# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "vasypd/map.jinja" import vasypd_settings with context %}

{# =================================
 # Remediate Vasyp vulnerabilities
 # ================================#}
{% if salt['cmd.run']('rpm -qa | grep -i vasyp') %}
validate_vasyp_pkg:
  test.succeed_without_changes:
    - Name: "VASYP not found this system. No vulnerabilities to remediate."

{% else %}
stop_vasyp_service:
  service.dead:
    - name: {{ vasypd_settings.service.name }}
    - unless:
      - rpm -q {{ vasypd_settings.pkg.name }}

remove_vasypd_pkg:
  pkg.purged:
    - name: {{ vasypd_settings.pkg.name }}
    - require:
      - service: stop_vasyp_service

{# Install vasyp #}

update_mount_policy_remove:
  cmd.run:
    - name: sed -i '/^\export\/home \/etc\/home.map/d' /etc/auto.master
    - onlyif:
      - grep '/export/home' /etc/auto.master

copy_map_file:
  file.managed:
    - name: {{ vasypd_settings.config.name }}
    - source: {{ vasypd_settings.config.source }}
    - mode: 0644
    - owner: root
    - group: root
    
restart_autofs_service_remove:
  service.running:
    - name: {{ vasypd_settings.pkg.name }}
    - reload: True
    - watch:
      - file: copy_map_file_remove
