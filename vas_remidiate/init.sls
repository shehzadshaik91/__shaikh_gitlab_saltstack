# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "vasypd/map.jinja" import vasypd_settings with context %}

{# =============================
 # Uninstall vasypd sevice
 # ============================#}
remove_vasypd_pkg:
  pkg.purged:
    - name: {{ vasypd_settings.pkg.name }}
    - unless:
      - rpm -q {{ vasypd_settings.pkg.name }}

{# TODO: update /etc/auto.master file
update_mount_policy_remove:
  file.replaceblock:
    - name: /etc/auto.master
#}

copy_map_file_remove:
  file.managed:
    - name: {{ vasypd_settings.config.name }}
    - source: {{ vasypd_settings.config.source }}
    - mode: 0644
    - owner: root
    - group: root

restart_autofs_service_remove:
  service.running:
    - name: {{ vasypd_settings.pkg.name }}
    - reload: True
    - watch:
      - file: copy_map_file_remove

{# =============================
 # Install vasypd sevice
 # ============================#}
install_vaspyd_service:
  pkg.installed:
    - name: {{ vasypd_settings.pkg.name }}

copy_map_file_install:
  file.managed:
    - name: {{ vasypd_settings.config.name }}
    - source: {{ vasypd_settings.config.source }}
    - mode: 0644
    - owner: root
    - group: root

{# TODO: update /etc/auto.master file
update_mount_policy_install:
  file.replaceblock:
    - name: /etc/auto.master
#}

{% for service in vasypd_settings.service.name %}
restart_autofs_service_install:
  service.running:
    - name: {{ vasypd_settings.service.name }}
    {% vasypd_settings.service.name == "autofs" %}
    - reload: {{ vasypd_settings.service.reload }}
    - watch:
      - file: copy_map_file_install
    {% endif %}
{% endfor %}
