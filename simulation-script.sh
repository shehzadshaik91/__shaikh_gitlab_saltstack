#!/bin/bash
#
#
echo "------------------------- BANER ------------------------"
echo "IP...............: $(hostname -I)"
echo "Username.........: $(whoami)"
echo "Date.............: $(date)"
echo "CPU Usage........: $(grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage ""}')"
echo "RAM Usage........: $(free -m | awk 'NR==2{printf "Memory Usage:%s/%sMB (%.2f%%)\n", $3,$2,$3*100/$2 }' | awk -F":" '{print $2}')"
echo "Disk Usage.......: $(df -h | awk '$NF=="/"{printf "Disk Usage:%d/%dGB (%s)\n", $3,$2,$5}' | awk -F":" '{print $2}')"
